<ul class="list-unstyled napr">
<?php
wp_reset_query();

$args = array(
    'post_type' => 'conference',
    'post_status' => 'publish',
);

query_posts($args);

if ( have_posts() ) :
    ?>
    <?php $c = 0 ?>
    <?php while (have_posts()) : ?>
    <?php $c++ ?>
    <?php the_post(); ?>
    <li>
        <a href="<?php the_permalink(); ?>"><?php echo the_title() ?></a>
    </li>
    <?php if ($c % 6 == 0) :  ?>

        <?php {
            break 1;
        } ?>
    <?php endif; ?>
<?php endwhile; ?>

<?php endif; ?>
<?php wp_reset_query(); ?>
</ul>
