<?php
    wp_reset_query();

    $args = array(
           'post_type' => 'news',
           'post_status' => 'publish',
       );

    query_posts($args);

    if ( have_posts() ) :
    ?>
                <?php $c = 0 ?>
                <?php while (have_posts()) : ?>
                    <?php get_template_part('news_list') ?>
                    <?php $c++ ?>
                    <?php if ($c % 3 == 0) :  ?>

                        <?php {
                            break 1;
                            } ?>
                    <?php endif; ?>
                <?php endwhile; ?>

        <?php endif; ?>
<?php wp_reset_query(); ?>
<button class="btn btn_custom btn_blue podpiska fll">Подписаться</button>
<a href="<?php echo get_post_type_archive_link('news'); ?>" class="btn btn_custom btn_blue flr">Все новости</a>
