<?php get_header(); ?>
<div class="container-fluid slider_wrapper">
    <div class="row">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                // Определение запроса
                $args = array(
                    'post_type' => 'slider',
                );
                $query = new WP_Query( $args );
                if ($query->have_posts()) {
                    $c = 0;
                    ?>

                        <?php while ( $query->have_posts() ) : $query -> the_post(); ?>
                        <?php $c++ ?>
                        <?php if ($c == 1) :  ?>
                        <div class="item active" style="background-image: url(<?php echo get_the_post_thumbnail_url($id, 'full'); ?>)">
                        <?php endif; ?>
                        <?php if ($c != 1) :  ?>
                        <div class="item" style="background-image: url(<?php echo get_the_post_thumbnail_url($id, 'full'); ?>)">
                        <?php endif; ?>
                            <div class="container caption_wrapper">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="carousel-caption left">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile;
                        ?>
                    <?php
                }
                wp_reset_postdata();
                ?>

                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>

  <div class="container-fluid main_content_wrapper">
      <div class="row">
          <div class="container main_content">
              <div class="row icon_block">
                  <div class="col-xs-4 icon_col">
                      <a href="#">
                          <img src="<?php bloginfo('template_url'); ?>/img/laptop-computer.png" alt="lap" class="img-responsive">
                          <span>Дистанционное обучение онлайн</span>
                      </a>
                  </div>
                  <div class="col-xs-4 icon_col">
                      <a href="#">
                          <img src="<?php bloginfo('template_url'); ?>/img/time.png" alt="lap" class="img-responsive">
                          <span>Гибкий график обучения</span>
                      </a>
                  </div>
                  <div class="col-xs-4 icon_col">
                      <a href="#">
                          <img src="<?php bloginfo('template_url'); ?>/img/desk.png" alt="lap" class="img-responsive">
                          <span>Большой выбор учебных программ</span>
                      </a>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-8 col-md-push-4">
                      <?php wp_reset_query(); ?>
                      <h3 class="has_border">О нас</h3>
                      <?php the_content() ?>
                 </div>
                  <div class="col-md-4 col-md-pull-8 news_col">
                      <h3 class="has_border">Новости</h3>
                      <?php get_template_part('sidebar_news'); ?>
                  </div>
              </div>
              <div class="row tile_block">
                  <div class="col-md-4">
                      <a href="/conference" class="tile">
                          <img src="<?php bloginfo('template_url'); ?>/img/seminar.jpg" alt="" class="img-responsive">
                    <span class="desc">
                        <span>Конференции</span>
                    </span>
                      </a>
                      <?php get_template_part('conference_list'); ?>
                  </div>
                  <div class="col-md-4">
                      <a href="/raise" class="tile">
                          <img src="<?php bloginfo('template_url'); ?>/img/seminar.jpg" alt="" class="img-responsive">
                    <span class="desc">
                        <span>Повышение кфалификации</span>
                    </span>
                      </a>
                      <?php get_template_part('up_list'); ?>
                  </div>
                  <div class="col-md-4">
                      <a href="/retraining" class="tile">
                          <img src="<?php bloginfo('template_url'); ?>/img/seminar.jpg" alt="" class="img-responsive">
                    <span class="desc">
                        <span>Переподготовка</span>
                    </span>
                      </a>
                      <?php get_template_part('vector_list'); ?>
                  </div>
              </div>
              <div class="row banner_row">
                  <div class="col-md-12">
                      <ul class="list-unstyled list-inline nav-justified banners_ul">
                          <li><a href="#" class="banner"><img src="<?php bloginfo('template_url'); ?>/img/ban_bot1_clr.gif" alt="banner" class="img-responsive"/></a></li>
                          <li><a href="#" class="banner"><img src="<?php bloginfo('template_url'); ?>/img/ban_bot2_clr.gif" alt="banner" class="img-responsive"/></a></li>
                          <li><a href="#" class="banner"><img src="<?php bloginfo('template_url'); ?>/img/ban_bot3_clr.gif" alt="banner" class="img-responsive"/></a></li>
                          <li><a href="#" class="banner"><img src="<?php bloginfo('template_url'); ?>/img/ban_bot5_clr.gif" alt="banner" class="img-responsive"/></a></li>
                          <li><a href="#" class="banner"><img src="<?php bloginfo('template_url'); ?>/img/ban_bot6_clr.gif" alt="banner" class="img-responsive"/></a></li>
                          <li><a href="#" class="banner"><img src="<?php bloginfo('template_url'); ?>/img/ban_bot8_clr.gif" alt="banner" class="img-responsive"/></a></li>
                          <li><a href="#" class="banner"><img src="<?php bloginfo('template_url'); ?>/img/ban_bot9_clr.gif" alt="banner" class="img-responsive"/></a></li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
</div>

<?php get_footer(); ?>