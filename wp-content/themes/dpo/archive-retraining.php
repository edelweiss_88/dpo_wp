<?php get_header(); ?>
<?php
// получение текущего запрашиваемого термина таксономии, чтобы позже использовать его в файле шаблона
$term = get_queried_object();
?>

<div class="container-fluid main_content_wrapper">
    <div class="row">
        <div class="container main_content">
            <div class="row">
                <div class="col-md-4 news_col">
                    <h3 class="has_border">Направления</h3>
                    <?php get_template_part('vector_list'); ?>

                    <h3 class="has_border">Новости</h3>
                    <?php get_template_part('sidebar_news'); ?>

                </div>
                <div class="col-md-8">
                    <h3 class="has_border">Переподготовка</h3>
                    <div class="row">
                    <?php
                    $terms = get_terms("vector");
                    $count = count($terms);
                    if($count >= 0) {
                        $c = 0;
                        foreach ($terms as $term) {
                            $c++;
                            ?>
                                <div class="col-md-4">
                                    <a href="<?php echo get_term_link($term, 'vector'); ?>" class="vector_link_wrapper">
                                        <?php
                                        $image = get_field("изображение",$term);
                                        if( !empty($image) ):
                                            $url = $image['url'];
                                            $title = $image['title'];
                                            $alt = $image['alt'];
                                            $caption = $image['caption'];
                                            $size = 'tax-thumb';
                                            $thumb = $image['sizes'][ $size ]; ?>
                                            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" class="img-responsive" />
                                        <?php endif; ?>
                                    <span class="vector_link"><?php echo $term->name; ?></span>
                                    </a>
                                </div>
                            <?php if ($c % 3 == 0) :  ?>
                            </div>
                            <div class="row">
                            <?php endif; ?>
                            <?php
                        }
                        wp_reset_postdata();
                    }
                    ?>
                            </div>
                            <?php
                            $page_id = 94;
                            $page_data = get_page( $page_id );
                            print apply_filters('the_content', $page_data->post_content);
                            ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>