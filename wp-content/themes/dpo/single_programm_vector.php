<li>
    <a href="<?php the_permalink(); ?>" class="sp_wrapper">
        <?php echo get_the_post_thumbnail($id, 'programm-thumb'); ?>
        <span class="description_wrapper"><?php the_title(); ?></span>
        <span><span class="in">Вид обучения: </span><?php echo $meta_values_date = get_post_meta($id, 'вид_обучения', true); ?></span>
        <span><span class="in">Количество часов: </span><?php echo $meta_values_date = get_post_meta($id, 'кол-во_академи-ческих_часов', true); ?></span>
        <span><span class="in">Выдаваемый документ: </span> <?php echo $meta_values_date = get_post_meta($id, 'выдаваемый_документ', true); ?></span>
        <?php if(get_post_meta($id, 'цена', true) != '') { ?>
            <span><span class="in">Цена: </span> <?php echo $meta_values_date = get_post_meta($id, 'цена', true); ?></span>
        <?php } ?>
        <?php if(get_post_meta($id, 'старая_цена', true) != '') { ?>
            <span class="opw">
                <span class="in">Старая цена:</span><span class="old_price"> <?php echo $meta_values_date = get_post_meta($id, 'старая_цена', true); ?></span>
            </span>
        <?php } ?>
        <?php if(get_post_meta($id, 'новая_цена', true) != '') { ?>
            <span class="opw">
                <span class="in">Новая цена:</span><span class="new_price"> <?php echo $meta_values_date = get_post_meta($id, 'новая_цена', true); ?></span>
            </span>
            <span><span class="in">Ближайший набор: </span> <?php setlocale(LC_ALL, 'ru_RU.UTF-8');
                echo strftime('%B');?>, <?php
                echo strftime('%B', strtotime("+1 month"));
                ?></span>
        <?php } ?>
    </a>
</li>

