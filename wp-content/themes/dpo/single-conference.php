<?php get_header(); ?>
    <div class="container-fluid main_content_wrapper">
        <div class="row">
            <div class="container main_content">
                <div class="row">
                    <div class="col-md-8 col-md-push-4">
                        <?php wp_reset_query(); ?>
                        <h3 class="has_border"><?php the_title(); ?></h3>
                        <div class="sp_wrapper" id="programm_description">
                            <span><span class="in">Шифр: </span><?php echo $meta_values_date = get_post_meta($id, 'шифр', true); ?></span>
                            <span><span class="in">Дата: </span><?php echo $meta_values_date = get_post_meta($id, 'дата', true); ?></span>
                            <?php if(get_post_meta($id, 'статус', true) == 'Завершена') { ?>
                                <span><span class="in">Статус: </span>Завершена</span>
                            <?php } ?>
                            <span><span class="in">Информационное письмо: </span><?php $file = get_field('письмо');
                                if( $file ): ?>
                                    <a href="<?php echo $file; ?>">Скачать</a>
                                <?php endif; ?>
                            </span>
                            <?php if(get_post_meta($id, 'статус', true) == 'Завершена') { ?>
                                <span><span class="in">Отчет: </span>
                                    <?php $file_otch = get_field('отчет');
                                    if( $file_otch ): ?>
                                        <a href="<?php echo $file_otch; ?>">Скачать</a>
                                    <?php endif; ?>
                            </span>
                            <?php } ?>

                            <?php if(get_post_meta($id, 'статус', true) != 'Завершена') { ?>
                                <span><a href="#for_anchor">Подать заявку</a></span>
                            <?php } ?>
                        </div>

                        <div class="content">
                            <img src="<?php echo get_the_post_thumbnail_url($id, 'full'); ?>" alt="" class="img-responsive alignleft programm_img"/>
                            <?php the_content(); ?>
                        </div>
                        <div class="collapse_wrapper">
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                Организационный комитет
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php
                                            $page_id = 108;
                                            $page_data = get_page( $page_id );
                                            print apply_filters('the_content', $page_data->post_content);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                Условия оплаты
                                                <script>
                                                    $(function () {
                                                        // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
                                                        $('[data-toggle="tooltip"]').tooltip({
                                                            title : '* с учетом скидки для постоянных авторов (Постоянно действует скидка 20% на публикацию любого количества статей ' +
                                                            'для постоянных авторов. Постоянными авторами мы считаем тех, кто уже опубликовал хотя бы одну статью!)',
                                                        })
                                                    })
                                                </script>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php
                                            $page_id = 110;
                                            $page_data = get_page( $page_id );
                                            print apply_filters('the_content', $page_data->post_content);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                               Реквизиты для оплаты публикации статьи
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php
                                            $page_id = 119;
                                            $page_data = get_page( $page_id );
                                            print apply_filters('the_content', $page_data->post_content);
                                            ?>                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                Правила оформления статьи
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php
                                            $page_id = 124;
                                            $page_data = get_page( $page_id );
                                            print apply_filters('the_content', $page_data->post_content);
                                            ?>                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(get_post_meta($id, 'статус', true) != 'Завершена') { ?>
                            <div class="form_wrapper" id="for_anchor">
                                <?php echo do_shortcode( '[contact-form-7 id="104" title="Запись на конференцию"]' ); ?>
                                <script>
                                    jQuery(function(){
                                        jQuery('input.title').val(<?= json_encode(get_the_title()) ?>);
                                    });
                                </script>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="col-md-4 col-md-pull-8 news_col">
                        <h3 class="has_border">Конференции</h3>
                        <?php get_template_part('conference_list'); ?>

                        <h3 class="has_border">Новости</h3>
                        <?php get_template_part('sidebar_news'); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(function(){
            jQuery('input.title_field').val(<?= json_encode(get_the_title()) ?>);
        });
    </script>
<?php get_footer(); ?>