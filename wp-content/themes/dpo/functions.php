<?php
function enqueue_styles() {
    wp_enqueue_style( 'dpo', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'lg', get_template_directory_uri() . '/css/lg.min.css');
    wp_enqueue_style( 'dpo-style', get_stylesheet_uri());
    wp_register_style('font-style', 'https://fonts.googleapis.com/css?family=Ubuntu:400,700,300,500&subset=latin,cyrillic-ext');
    wp_enqueue_style( 'font-style');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_scripts () {
    wp_register_script('html5-shim', 'http://html5shim.googlecode.com/svn/trunk/html5.js');
    wp_enqueue_script('html5-shim');
    wp_enqueue_script('jquery-1.12.0.min', get_template_directory_uri() . '/js/jquery-1.12.0.min.js');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js');
    wp_enqueue_script('lg.min', get_template_directory_uri() . '/js/lg.min.js');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

function remove_menus(){
    remove_menu_page( 'edit.php' );                   //Записи
    remove_menu_page( 'edit-comments.php' );          //Комментарии
}

add_action( 'admin_menu', 'remove_menus' );

add_filter( 'upload_size_limit', 'PBP_increase_upload' );
function PBP_increase_upload( $bytes )
{
    return 10485760; // 1 megabyte
}
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 ); // размер миниатюры поста по умолчанию
}

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'programm-thumb', 250, 150, true ); // Кадрирование изображения
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'news-thumb', 110, 70, true ); // Кадрирование изображения
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'tax-thumb', 220, 150, true ); // Кадрирование изображения
}

add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'retraining',
        array(
            'labels' => array(
                'name' => 'Переподготовка',
                'singular_name' =>  'Переподготовка',
                'add_new' => 'Добавить',
                'add_new_item' =>'Добавить',
                'edit_item' => 'Редактировать',
                'new_item' => 'Новая',
            ),
            'public' => true,
            'has_archive' => true,
            'hierarchical' => true,
            'capability_type' => 'post',
            'supports' => array( 'title', 'editor', 'thumbnail', 'category',),
            'menu_icon'   => 'dashicons-id',
        )
    );
    register_post_type( 'raise',
        array(
            'labels' => array(
                'name' => 'Повышение',
                'singular_name' =>  'Повышение',
                'add_new' => 'Добавить',
                'add_new_item' =>'Добавить',
                'edit_item' => 'Редактировать',
                'new_item' => 'Новая',
            ),
            'public' => true,
            'has_archive' => true,
            'hierarchical' => true,
            'capability_type' => 'page',
            'supports' => array( 'title', 'editor', 'thumbnail', 'category',),
            'menu_icon'   => 'dashicons-chart-bar',
        )
    );
    register_post_type( 'conference',
        array(
            'labels' => array(
                'name' => 'Конференции',
                'singular_name' =>  'Конференция',
                'add_new' => 'Добавить Конференцию',
                'add_new_item' =>'Добавить Конференцию',
                'edit_item' => 'Редактировать Конференцию',
                'new_item' => 'Новый Конференция',
            ),
            'public' => true,
            'has_archive' => true,
            'hierarchical' => true,
            'capability_type' => 'post',
            'supports' => array( 'title', 'editor', 'thumbnail',),
            'menu_icon'   => 'dashicons-groups',
        )
    );
    register_post_type( 'news',
        array(
            'labels' => array(
                'name' => 'Новости',
                'singular_name' =>  'Новость',
                'add_new' => 'Добавить Новость',
                'add_new_item' =>'Добавить Новость',
                'edit_item' => 'Редактировать Новость',
                'new_item' => 'Новая Новость',
            ),
            'public' => true,
            'has_archive' => true,
            'hierarchical' => true,
            'capability_type' => 'post',
            'supports' => array( 'title', 'editor', 'thumbnail',),
            'menu_icon'   => 'dashicons-format-aside',
        )
    );
    register_post_type( 'slider',
        array(
            'labels' => array(
                'name' => 'Слайдер',
                'singular_name' =>  'Слайдер',
                'add_new' => 'Добавить Слайдер',
                'add_new_item' =>'Добавить Слайдер',
                'edit_item' => 'Редактировать Слайдер',
                'new_item' => 'Новый Слайдер',
            ),
            'public' => true,
            'has_archive' => true,
            'hierarchical' => true,
            'capability_type' => 'post',
            'supports' => array( 'editor', 'thumbnail',),
            'menu_icon'   => 'dashicons-images-alt2',
        )
    );
    flush_rewrite_rules();
}

function news_permalink($permalink, $post = 0){
    if ( $post->post_type == 'news' ){
        return $permalink .$post->ID .'/';
    } else {
        return $permalink;
    }
}
add_filter('post_type_link', 'news_permalink', 1, 3);

function news_rewrite_init(){
    add_rewrite_rule(
        'news/.+?/([0-9]+)?$',
        'index.php?post_type=news&p=$matches[1]',
        'top'
    );
}
add_action( 'init', 'news_rewrite_init');
// хук через который подключается функция
// регистрирующая новые таксономии (create_book_taxonomies)
add_action( 'init', 'create_retrtax_taxonomies', 0 );

function create_retrtax_taxonomies(){
    $labels = array(
        'name' => _x( 'Направление', 'taxonomy general name' ),
        'singular_name' => _x( 'Направление', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Направление' ),
        'all_items' => __( 'All Направление' ),
        'parent_item' => __( 'Parent Направление' ),
        'parent_item_colon' => __( 'Parent Направление:' ),
        'edit_item' => __( 'Edit Направление' ),
        'update_item' => __( 'Update Направление' ),
        'add_new_item' => __( 'Add New Направление' ),
        'new_item_name' => __( 'New Направление Name' ),
        'menu_name' => __( 'Направление' ),
    );

    register_taxonomy('vector', array('retraining'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'vector' ),
    ));

}
add_action( 'init', 'create_raisetax_taxonomies', 0 );

function create_raisetax_taxonomies(){
    $labels = array(
        'name' => _x( 'Направление', 'taxonomy general name' ),
        'singular_name' => _x( 'Направление', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Направление' ),
        'all_items' => __( 'All Направление' ),
        'parent_item' => __( 'Parent Направление' ),
        'parent_item_colon' => __( 'Parent Направление:' ),
        'edit_item' => __( 'Edit Направление' ),
        'update_item' => __( 'Update Направление' ),
        'add_new_item' => __( 'Add New Направление' ),
        'new_item_name' => __( 'New Направление Name' ),
        'menu_name' => __( 'Направление' ),
    );

    register_taxonomy('up', array('raise'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'up' ),
    ));

}

//add_action( 'pre_get_posts', 'my_change_sort_order');
//function my_change_sort_order($query){
//    if(is_post_type_archive('conference')){
//        //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
//        //Set the order ASC or DESC
//        $query->set( 'order', 'DESC' );
//        //Set the orderby
//        $query->set( 'orderby', 'meta_value' );
//        $query->set('meta_key', 'статус');
//    }
//};
