<?php get_header(); ?>

    <div class="container-fluid main_content_wrapper">
        <div class="row">
            <div class="container main_content">
                <div class="row">
                    <div class="col-md-8 col-md-push-4">
                        <?php wp_reset_query(); ?>
                        <h3 class="has_border"><?php the_title(); ?></h3>

                        <div class="content content_news">
                            <img src="<?php echo get_the_post_thumbnail_url($id, 'full'); ?>" alt="" class="img-responsive alignleft programm_img"/>
                            <?php the_content(); ?>
                            <div class="test"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-pull-8 news_col">
                        <h3 class="has_border">Новости</h3>
                        <?php get_template_part('sidebar_news'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function() {

            $('.content_news').find('[id^="gallery"].gallery').each(function(){
                var a = 'lb_' + $(this).attr('id');
                $(this).find('br').remove();
                $(this).find('img').css({"border": "0"});
                console.log(a)
                $(this).append($('<div id=' + a + ' class="page_gall"></div>'));
                $(this).find(".gallery-item a").appendTo($('#' + a + ''));
                $(this).find(".gallery-item").each(function() {
                   $(this).remove();
                })
            });
            $('[id^="lb"]').lightGallery();
        });

    </script>

<?php get_footer(); ?>