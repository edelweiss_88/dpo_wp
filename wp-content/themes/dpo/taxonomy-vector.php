<?php get_header(); ?>
<?php
// получение текущего запрашиваемого термина таксономии, чтобы позже использовать его в файле шаблона
$term = get_queried_object();
?>

<div class="container-fluid main_content_wrapper">
    <div class="row">
        <div class="container main_content">
            <div class="row">
                <div class="col-md-4 news_col">
                    <h3 class="has_border">Направления</h3>
                    <?php get_template_part('vector_list'); ?>

                    <h3 class="has_border">Новости</h3>
                    <?php get_template_part('sidebar_news'); ?>
                </div>
                <div class="col-md-8">
                    <h3 class="has_border"><?php echo $term->name; ?></h3>
                    <?php
                    // Определение запроса
                    $args = array(
                        'post_type' => 'retraining',
                        'vector' => $term->slug
                    );
                    $query = new WP_Query( $args );
                    if ($query->have_posts()) {

                        ?>
                        <ul class="list-unstyled">

                            <?php while ( $query->have_posts() ) : $query -> the_post(); ?>
                                <?php get_template_part('single_programm_vector'); ?>

                            <?php endwhile;
                            ?>
                        </ul>
                        <?php
                    }
                    wp_reset_postdata();
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
