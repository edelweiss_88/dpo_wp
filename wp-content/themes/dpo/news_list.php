<?php the_post(); ?>

<a href="<?php the_permalink(); ?>" class="news_wrapper">
    <?php
        if( has_post_thumbnail() ) {
          ?>  <img src="<?php echo get_the_post_thumbnail_url($id, 'news_thumb'); ?>" alt="" class="img-responsive"/> <?php
        } else {
            echo '<img src="'.get_bloginfo("template_url").'/img/img-default.jpg" class="img-responsive"/>';
        }
    ?>
    <span class="title_wrapper">
        <span class="date"><?php the_time('j F Y'); ?></span>
        <span class="title"><?php the_title(); ?></span>
    </span>
</a>