<html lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" href="css/media.css" media="(max-width: 768px)"/>-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="social">
    <script type="text/javascript">(function() {
            if (window.pluso)if (typeof window.pluso.start == "function") return;
            if (window.ifpluso==undefined) { window.ifpluso = 1;
                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                var h=d[g]('body')[0];
                h.appendChild(s);
            }})();</script>
    <div class="pluso" data-background="transparent" data-options="small,round,line,vertical,counter,theme=04" data-services="facebook,vkontakte,odnoklassniki"></div>
</div>
<div class="container-fliud header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="/" class="logo_wrapper">
                    <img src="<?php bloginfo('template_url'); ?>/img/logo2.jpg" alt="logo" class="img-responsive">
                    <span>Дополнительное профессиональное<br>образование - онлайн</span>
                </a>
            </div>
            <div class="col-md-6">
                <div class="phone_wrapper">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <span class="phone"><phone>0-800-000-000</phone></span>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu_collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="menu_collapse">
            <?php
            require_once('wp_bootstrap_navwalker.php');

            wp_nav_menu(array(
                'menu' => 'top_menu',
                'menu_class' => 'nav nav-justified',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker()
            ));
            ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="popup_wrapper">
    <div class="popup">
        <?php echo do_shortcode('[contact-form-7 id="92" title="Подписаться"]') ?>
        <div class="close">X</div>
    </div>
</div>
