<?php get_header(); ?>

<div class="container-fluid main_content_wrapper">
    <div class="row">
        <div class="container main_content">
            <div class="row">
                <div class="col-md-4 news_col">
                    <h3 class="has_border">Конференции</h3>
                    <?php get_template_part('conference_list'); ?>

                    <h3 class="has_border">Новости</h3>
                    <?php get_template_part('sidebar_news'); ?>

                </div>
                <div class="col-md-8">
                    <h3 class="has_border">График конференций</h3>
                    <table class="table table-responsive" id="conf_table">
                    <thead>
                        <tr>
                            <th>Шифр</th>
                            <th>Дата</th>
                            <th>Название конференции</th>
                            <th>Информационное письмо<br>Подать заявку</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $the_key = 'статус';  // The meta key to sort on
                    $args = array(
                        'meta_key' => $the_key,
                        'orderby'  => array(
                            'meta_value' => 'ASC',
                            'post_date'      => 'DESC',
                        ),
                    );
                    global $wp_query;
                    query_posts(
                        array_merge(
                            $wp_query->query,
                            $args
                        )
                    );
                    ?>
                    <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : ?>
                        <?php the_post(); ?>
                        <tr class="<?php if(get_post_meta($id, 'статус', true) == 'Завершена') { echo ('zav'); } ?>">
                            <td><?php echo $meta_values_date = get_post_meta($id, 'шифр', true); ?></td>
                            <td>
                            <?php echo $meta_values_date = get_post_meta($id, 'дата', true); ?>
                            <?php if(get_post_meta($id, 'статус', true) == 'Завершена') { ?>
                                <br><span>Проведена</span>
                                <?php } ?>
                            </td>
                            <td><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></td>
                            <td>
                            <?php if(get_post_meta($id, 'статус', true) == 'Завершена') { ?>
                                    <?php $file_otch = get_field('отчет');
                                    if( $file_otch ): ?>
                                        <a href="<?php echo $file_otch; ?>">Скачать отчет</a>
                                    <?php endif; ?>
                            <?php } ?>
                                <?php if(get_post_meta($id, 'статус', true) != 'Завершена') { ?>
                                    <?php $file = get_field('письмо');
                                    if( $file ): ?>
                                        <a href="<?php echo $file; ?>">Скачать</a>
                                    <?php endif; ?><br>
                                <?php } ?>
                                <?php if(get_post_meta($id, 'статус', true) != 'Завершена') { ?>
                                <a href="<?php the_permalink(); ?>#for_anchor">Подать заявку</a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    </tbody>
                    </table>
                    <?php
                            $page_id = 105;
                            $page_data = get_page( $page_id );
                            print apply_filters('the_content', $page_data->post_content);
                            ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>

