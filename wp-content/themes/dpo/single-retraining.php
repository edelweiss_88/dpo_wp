<?php get_header(); ?>
<?php
// получение текущего запрашиваемого термина таксономии, чтобы позже использовать его в файле шаблона
$term = get_queried_object();
?>

<div class="container-fluid main_content_wrapper">
    <div class="row">
        <div class="container main_content">
            <div class="row">
                <div class="col-md-8 col-md-push-4">
                    <?php wp_reset_query(); ?>
                    <h3 class="has_border"><?php the_title(); ?></h3>
                    <?php the_terms( $post->ID, 'vector', '<p> <strong>Направление: </strong> ', ' ','</p>' ); ?>
                    <div class="sp_wrapper" id="programm_description">
                        <span><span class="in">Вид обучения: </span><?php echo $meta_values_date = get_post_meta($id, 'вид_обучения', true); ?></span>
                        <span><span class="in">Количество часов: </span><?php echo $meta_values_date = get_post_meta($id, 'кол-во_академи-ческих_часов', true); ?></span>
                        <span><span class="in">Выдаваемый документ: </span> <?php echo $meta_values_date = get_post_meta($id, 'выдаваемый_документ', true); ?></span>
                        <?php if(get_post_meta($id, 'цена', true) != '') { ?>
                            <span><span class="in">Цена: </span> <?php echo $meta_values_date = get_post_meta($id, 'цена', true); ?></span>
                        <?php } ?>
                        <?php if(get_post_meta($id, 'старая_цена', true) != '') { ?>
                            <span class="opw">
                <span class="in">Старая цена:</span><span class="old_price"> <?php echo $meta_values_date = get_post_meta($id, 'старая_цена', true); ?></span>
            </span>
                        <?php } ?>
                        <?php if(get_post_meta($id, 'новая_цена', true) != '') { ?>
                            <span class="opw">
                <span class="in">Новая цена:</span><span class="new_price"> <?php echo $meta_values_date = get_post_meta($id, 'новая_цена', true); ?></span>
            </span>
                        <?php } ?>
                        <span><span class="in">Ближайший набор: </span> <?php setlocale(LC_ALL, 'ru_RU.UTF-8');
                            echo strftime('%B');?>, <?php
                            echo strftime('%B', strtotime("+1 month"));
                            ?></span>
                    </div>

                    <div class="content">
                        <ul class="nav nav-tabs nav-justified nav_programm">
                            <li class="active"><a href="#home" data-toggle="tab">Описание</a></li>
                            <li><a href="#plan" data-toggle="tab">Учебный план</a></li>
                            <li><a href="#profile" data-toggle="tab">Диплом</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                                <img src="<?php echo get_the_post_thumbnail_url($id, 'full'); ?>" alt="" class="img-responsive alignleft programm_img"/>
                                <?php the_content(); ?>                            </div>
                            <div class="tab-pane" id="plan">
                                <div id="lightgallery_plan">
                                    <?php if(get_field("учебный_план") ) { ?>
                                    <a href="<?php echo get_field("учебный_план"); ?>"><img src="<?php echo get_field("учебный_план"); ?>" alt="Учебный план" class="img-responsive"></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile">
                                <div id="lightgallery">
                                    <?php echo get_field("диплом"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form_wrapper">
                        <?php echo do_shortcode( '[contact-form-7 id="48" title="Записаться"]' ); ?>
                        <script>
                            jQuery(function(){
                                jQuery('input.title').val(<?= json_encode(get_the_title()) ?>);
                            });
                        </script>
                    </div>
                </div>
                <div class="col-md-4 col-md-pull-8 news_col">
                    <h3 class="has_border">Направления</h3>
                    <?php get_template_part('vector_list'); ?>

                    <h3 class="has_border">Новости</h3>
                    <?php get_template_part('sidebar_news'); ?>

                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        jQuery(function(){
            jQuery('input.title_field').val(<?= json_encode(get_the_title()) ?>);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#lightgallery p").lightGallery();
        });
        $(document).ready(function() {
            $("#lightgallery_plan").lightGallery();
        });
    </script>

<?php get_footer(); ?>