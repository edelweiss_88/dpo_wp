<?php
wp_reset_postdata();
$terms = get_terms("up");
$count = count($terms);
if($count >= 0){
    ?>
    <ul class="list-unstyled napr">
    <?php
    foreach ($terms as $term) {
        echo '<li><a href="'.get_term_link($term->slug, 'up').'">'.$term->name.'</a></li>';
    }
    ?></ul><?php
}
wp_reset_postdata();
?>