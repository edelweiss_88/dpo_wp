<?php get_header(); ?>

<div class="container-fluid main_content_wrapper">
    <div class="row">
        <div class="container main_content">
            <div class="row">
                <div class="col-md-8 col-md-push-4">
                    <?php wp_reset_query(); ?>
                    <h3 class="has_border"><?php the_title(); ?></h3>

                    <div class="content content_news">
                        <p class="date"><?php the_time('j F Y'); ?></p>
                        <img src="<?php echo get_the_post_thumbnail_url($id, 'full'); ?>" alt="" class="img-responsive alignleft programm_img"/>
                        <?php the_content(); ?>
                        <div class="line_helper"></div>
                        <button class="btn btn_custom btn_blue podpiska fll">Подписаться</button>
                        <a href="<?php echo get_post_type_archive_link('news'); ?>" class="btn btn_custom btn_blue flr">Все новости</a>
                    </div>
                </div>
                <div class="col-md-4 col-md-pull-8 news_col">
                    <h3 class="has_border">Новости</h3>
                    <?php get_template_part('sidebar_news'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>