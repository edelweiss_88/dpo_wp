<?php get_header(); ?>


<div class="container-fluid main_content_wrapper">
    <div class="container main_content">
        <div class="row">
            <div class="col-md-12">
                <?php wp_reset_query(); ?>
                <h3 class="has_border">Все новости</h3>
                <div class="row">
                    <?php if (have_posts()) : ?>
                    <?php $c = 0 ?>
                    <?php while (have_posts()) : ?>
                    <div class="col-md-4">
                        <?php get_template_part('news_list'); ?>
                    </div>
                    <?php $c++ ?>
                    <?php if ($c % 3 == 0) :  ?>
                </div>
                <div class="row">
                    <?php endif; ?>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="line_helper"></div>
                <button class="btn btn_custom btn_blue podpiska">Подписаться</button><br>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>