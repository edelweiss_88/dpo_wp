<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'dpo');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7jV?<nURA[LU/fHlpo-.z%/ulF[rl5If9SM$&nSG+2$j7w*p468OpX,k9}3B%(Nh');
define('SECURE_AUTH_KEY',  'K]U]H=jf(]j&!++2tlQV03LPX`BTOG-7V=8WnMa=+e VYen(D5pQ$1TjXe.fbIeM');
define('LOGGED_IN_KEY',    '=nOWH@+]&+Dn*X|&NhzCw#x?eT}Zv:-9(GwYJNhS7k*mEb-jNB/HVFwq@GZnL:m>');
define('NONCE_KEY',        '>&-6#f=E-Z~10|J+vf=c{q`Gzx6Y%OH?^2+*@iGD3Z+q;>9$KFZ0SG seYtT_IpB');
define('AUTH_SALT',        '[RyT~x9z@5T+ P-(5FPK9EHCjT:4@g8bh_+y%M}|b7@Dm_*UzZ4[lA$-gsS_^V^9');
define('SECURE_AUTH_SALT', 'W0kA,1/&_=`m.VEZcA|S~|Z|fQB|EZW-+J%9^Sb2ikDz,Qyn71Om}X`M+?]p-:*#');
define('LOGGED_IN_SALT',   'J|)sz|m4+f7fMNiyp;?pV#(&d2X|r/=ExTzDW,;82ipq3REjn9vWeSNf1kR43=mg');
define('NONCE_SALT',       'V6q,Q/g=zJ~g92J_L#DoL]bxf4eTSFp|@qGsH+?&oorzy)@LVdy|lj;s{tcl/mV+');

/**#@-*/
define('WP_MEMORY_LIMIT', '64MB');
/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
